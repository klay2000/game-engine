package Entities;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Character {

    BufferedImage spriteSheet;


    public Character(){
        spriteSheet.getGraphics().setColor(Color.ORANGE);
        spriteSheet.getGraphics().fillRect(10, 10, 10, 10);
        spriteSheet.getGraphics().dispose();
    }


    public BufferedImage getSpriteSheet (){
        return spriteSheet;
    }

}
