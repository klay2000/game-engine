package graphics;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glDisableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;

public class Model {

    private int draw_count; // number of vertices to draw
    private int v_id; // id of vertices
    private int t_id; // id of texture cords
    private int i_id; // id of indices
    protected Matrix4f modelMatrix = new Matrix4f(); // model matrix
    public Texture texture; // texture

    public Model(float[] vertices, float[] tex_coords, int[] indices, Texture texture){ // makes a new model based on the number of vertices
        this.texture = texture; // sets texture

        draw_count = indices.length; // sets draw count to number of vertices in vertices

        v_id = glGenBuffers(); // sets vertex id
        glBindBuffer(GL_ARRAY_BUFFER, v_id); // binds vertices
        glBufferData(GL_ARRAY_BUFFER, createFloatBuffer(vertices), GL_STATIC_DRAW); // sends vertex data to gpu

        t_id = glGenBuffers(); // sets texture id
        glBindBuffer(GL_ARRAY_BUFFER, t_id); // binds textue
        glBufferData(GL_ARRAY_BUFFER, createFloatBuffer(tex_coords), GL_STATIC_DRAW); // sends texture to gpu

        i_id = glGenBuffers(); // sets indices id
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, i_id); // binds indices
        IntBuffer indicesBuffer = BufferUtils.createIntBuffer(indices.length); // creates indices buffer
        indicesBuffer.put(indices); // fills buffer with indices
        indicesBuffer.flip(); // flips buffer
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_STATIC_DRAW); // sends indices to gpu

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbinds stuff
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); // unbinds more stuff
    }

    protected void render() {
        texture.bind(0); // binds texture

        glEnableVertexAttribArray(0); // enables attribute
        glEnableVertexAttribArray(1); // enables attribute

        glBindBuffer(GL_ARRAY_BUFFER, v_id); // binds vertices
        glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0); // sets vertices to go to appropriate attribute in shader

        glBindBuffer(GL_ARRAY_BUFFER, t_id); // binds textures
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0); // sets texture coords to go to appropriate attribute in shader

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, i_id); // binds indices
        glDrawElements(GL_TRIANGLES, draw_count, GL_UNSIGNED_INT, 0); // actually sends everything to shader

        glBindBuffer(GL_ARRAY_BUFFER, 0); // unbinds all
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); // unbinds indices

        glDisableVertexAttribArray(0); // disables attribute
        glDisableVertexAttribArray(1); // disables attribute

        modelMatrix = new Matrix4f(); // resets model matrix
    }

    public void rotate(float degrees){ // rotates model matrix
        modelMatrix.rotate((float)Math.toRadians(degrees), 0, 0, 1);
    }

    public void translate(float x, float y){ // translates model matrix
        modelMatrix.translate(x, y, 0);

    }

    public void scale(float x, float y){ // scales model matrix
        modelMatrix.scale(x, y, 1);
    }

    private FloatBuffer createFloatBuffer(float[] array){ // makes float buffer from array
        FloatBuffer buffer = BufferUtils.createFloatBuffer(array.length); // makes float buffer with same length as array
        buffer.put(array); // fills float buffer with data from array
        buffer.flip(); // flips float buffer because openGL likes that
        return buffer;
    }

}