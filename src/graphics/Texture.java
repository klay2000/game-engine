package graphics;

import org.lwjgl.BufferUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class Texture {

    private int id; // textures id
    private int width; // width of texture
    private int height; // height of texture

    public Texture(String fileName){ // creates texture from filename

        BufferedImage bi; // buffered image of texture

        try {
            bi= ImageIO.read(new File(fileName)); // reads in buffered image
            width = bi.getWidth(); // sets width
            height = bi.getHeight(); // sets height

            int[] pixelsRaw = bi.getRGB(0, 0, width, height, null, 0, width); /*sets pixelsRaw
            to pixels of buffered image*/

            ByteBuffer pixels = BufferUtils.createByteBuffer(width * height * 4); // initializes byte buffer

            for(int i = 0; i < width; i++){ // parses buffered image to float buffer
                for(int j = 0; j < height; j++){
                    int pixel = pixelsRaw[i*width+j];
                    pixels.put((byte) ((pixel >> 16) & 0xFF)); // red
                    pixels.put((byte) ((pixel >> 8) & 0xFF)); // green
                    pixels.put((byte) (pixel & 0xFF)); // blue
                    pixels.put((byte) ((pixel >> 24) & 0xFF)); // alpha
                }
            }

            pixels.flip(); // flips byte buffer

            id = glGenTextures(); // sets texture id
            glBindTexture(GL_TEXTURE_2D, id); // binds texture to id

            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // sets texture parameter
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // sets texture parameter

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels); // sets texture to byte buffer

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Texture(BufferedImage bi){ // creates texture from filename
            width = bi.getWidth(); // sets width
            height = bi.getHeight(); // sets height

            int[] pixelsRaw = bi.getRGB(0, 0, width, height, null, 0, width); /*sets pixelsRaw
            to pixels of buffered image*/

            ByteBuffer pixels = BufferUtils.createByteBuffer(width * height * 4); // initializes byte buffer

            for(int i = 0; i < width; i++){ // parses buffered image to float buffer
                for(int j = 0; j < height; j++){
                    int pixel = pixelsRaw[i*width+j];
                    pixels.put((byte) ((pixel >> 16) & 0xFF)); // red
                    pixels.put((byte) ((pixel >> 8) & 0xFF)); // green
                    pixels.put((byte) (pixel & 0xFF)); // blue
                    pixels.put((byte) ((pixel >> 24) & 0xFF)); // alpha
                }
            }

            pixels.flip(); // flips byte buffer

            id = glGenTextures(); // sets texture id
            glBindTexture(GL_TEXTURE_2D, id); // binds texture to id

            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // sets texture parameter
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // sets texture parameter

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels); // sets texture to byte buffer
    }

    protected void bind(int sampler){ // binds texture
        if(sampler >= 0 && sampler <= 31)
            glActiveTexture(GL_TEXTURE0 + sampler);
        glBindTexture(GL_TEXTURE_2D, id);
    }
}
