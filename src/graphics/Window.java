package graphics;

import org.joml.Matrix4f;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;

import java.util.ArrayList;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

public class Window implements Runnable {

    private static boolean isGLFWInitialized = false;

    private ArrayList<Model> modelsToRender = new ArrayList<>();

    private int width, height; // width and heght of framebuffer, and window if it isn't fullscreen

    private float aspectRatio; // aspect ratio of framebuffer
    private int aspectOffset; // offset to center drawn screen

    private Shader shader; // shader

    private long handle; // window handle

    public Camera camera; // camera

    public Window(int width, int height, GLFWKeyCallback keyCallback, String title, boolean isFullscreen) { /*creates new window based on width,
                                                                              height, keyCallback, title, and whether or not window is fullscreen*/
        if(!isGLFWInitialized) { // init GLFW if it isn't already and throw an error if it won't initialize
            if(!glfwInit())
                throw new IllegalStateException("GLFW won't initialize!");
            isGLFWInitialized = true;
        }

        camera = new Camera(width/2, height/2, width, height); // initializes window

        if(isFullscreen)
            handle = glfwCreateWindow(width, height, title, glfwGetPrimaryMonitor(), 0); // create a window in fullscreen mode and store its handle
        else
            handle = glfwCreateWindow(width, height, title, 0, 0); // create a window in windowed mode and store its handle

        glfwSetKeyCallback(handle, keyCallback); // sets keyCallback

        glfwShowWindow(handle); // show the window

        glfwMakeContextCurrent(handle); // make the handle look for a context

        GL.createCapabilities(); // create the context for the window

        glEnable(GL_TEXTURE_2D); // enables textures

        shader = new Shader("shader"); // initializes shader

        new Thread(this).start(); // starts window managment thread
    }


    public void drawFrame(){ // draw frame with aspect offset and ratio applied and then clear framebuffer

        glfwPollEvents(); // polls events

        glClear(GL_COLOR_BUFFER_BIT); // clears screen

        for (Model model : modelsToRender){ //scans through queued models and renders them
            shader.bind();
            shader.setUniform("sampler", 0);
            shader.setUniform("modelMatrix", model.modelMatrix);
            shader.setUniform("camMatrix", camera.getMatrix());
            model.render();
        }
        modelsToRender.clear(); // clears models queued

        glfwSwapBuffers(handle); // swaps buffers (basically displays frame
    }

    public void queueModel(Model model){ // adds models to rendering queue
        modelsToRender.add(model);
    }

    @Override
    public void run() { // checks to see if window must be closed every 10ms and closes it if need be as well as polls window
        while(true) {

            if (glfwWindowShouldClose(handle)) {
                glfwTerminate(); // terminates glfw
                System.exit(0); // exits program
            }

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
