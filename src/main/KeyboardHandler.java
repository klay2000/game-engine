package main;

import org.lwjgl.glfw.GLFWKeyCallback;

import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;

public class KeyboardHandler extends GLFWKeyCallback {
    public boolean[] keys = new boolean[65536]; // array of key stuff

    @Override
    public void invoke(long window, int key, int scancode, int action, int mods) { // method is called when a key event happens
        keys[key] = action != GLFW_RELEASE;
    }

    public boolean getKey(int keycode){ // gets key state

        return keys[keycode];

    }
}
