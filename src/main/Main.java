package main;

import graphics.Texture;
import graphics.Window;
import org.dyn4j.dynamics.World;
import org.dyn4j.geometry.Vector2;
import Entities.Character;
import org.lwjgl.glfw.GLFWKeyCallback;

import static org.lwjgl.glfw.GLFW.*;

public class Main {

    public static void main(String args[]){

        int width = 1920, height = 1080;

        KeyboardHandler keyHandler = new KeyboardHandler();

        Window window = new Window(width, height, keyHandler,"Platformer", true);

        World world = new World();

        world.setGravity(new Vector2(0, -9.8));

        Character test = new Character();

        GameObject gameObject = GameObject.makeCircle(100, 500, 500, 0, 0.7f, 0.5f, 0.0001f, world, new Texture(test.getSpriteSheet()), false);

        GameObject floor = GameObject.makeRectangle(width, 10, width/2, 10, 0, 0.7f, 0.5f, 11f, world, new Texture("./res/testtexture2.png"), true);

        System.out.println("loaded");

        while(true){

            if(keyHandler.getKey(GLFW_KEY_D))gameObject.applyTorque(-10000);
            if(keyHandler.getKey(GLFW_KEY_A))gameObject.applyTorque(10000);
            if(keyHandler.getKey(GLFW_KEY_W))gameObject.applyForce(new Vector2(0, 1000));

            gameObject.render(window);

            floor.render(window);

            window.drawFrame();

            world.step(10);

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }

}
