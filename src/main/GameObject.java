package main;

import graphics.Model;
import graphics.Quad;
import graphics.Texture;
import graphics.Window;
import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.World;
import org.dyn4j.geometry.*;

public class GameObject {

    private Model model; // gameObject's model
    private Body body; // gameObject's physics body

    // makes a circle shaped gameObject and returns it
    public static GameObject makeCircle(float radius, float x, float y, float rot, float restitution, float friction, float density, World world, Texture texture, boolean isStatic){
        GameObject gameObject = new GameObject(); // initializes gameObject

        gameObject.body = new Body(); // initializes body of gameObject

        gameObject.body.addFixture(new Circle(radius), density, friction, restitution); // adds fixture

        if(isStatic)gameObject.body.setMass(MassType.INFINITE); // sets mass to infinite if object is static, otherwise normal
        else gameObject.body.setMass(MassType.NORMAL);

        gameObject.body.translate(x, y); // translates body to x and y
        gameObject.body.rotate(Math.toDegrees(rot)); // rotates body

        gameObject.model = new Quad(0, 0, radius*2, radius*2, texture); // creates model

        world.addBody(gameObject.body); // adds body to world

        return gameObject; // returns gameObject
    }

    // makes a rectangle shaped gameObject and returns it
    public static GameObject makeRectangle(float width, float height, float x, float y, float rot, float restitution, float friction, float density, World world, Texture texture, boolean isStatic){
        GameObject gameObject = new GameObject(); // initializes gameObject

        gameObject.body = new Body(); // initializes body of gameObject

        gameObject.body.addFixture(new Rectangle(width, height), density, friction, restitution); // adds fixture

        if(isStatic)gameObject.body.setMass(MassType.INFINITE); // if static then make mass infinite, otherwise normal
        else gameObject.body.setMass(MassType.NORMAL);

        gameObject.body.translate(x, y); // translates body to x and y
        gameObject.body.rotate(Math.toDegrees(rot)); // rotates body

        gameObject.model = new Quad(0, 0, width, height, texture); // creates model

        world.addBody(gameObject.body); // adds body to world

        return gameObject; // returns gameObject
    }

    // makes a Triangle shaped gameObject and returns it
    public static GameObject makeTriangle(Vector2 point1, Vector2 point2, Vector2 point3, float x, float y, float rot, float restitution, float friction, float density, World world, Texture texture, boolean isStatic){
        GameObject gameObject = new GameObject(); // initializes gameObject

        gameObject.body = new Body(); // initializes body of gameObject

        gameObject.body.addFixture(new Triangle(point1, point2, point3), density, friction, restitution); // adds fixture

        if(isStatic)gameObject.body.setMass(MassType.INFINITE); // sets mass to infinite if object is static, otherwise normal
        else gameObject.body.setMass(MassType.NORMAL);

        gameObject.body.translate(x, y); // translates body to x and y
        gameObject.body.rotate(Math.toDegrees(rot)); // rotates body

        // determines largest and smallest points
        float largestX, smallestX, largestY, smallestY;
        if(point1.x > point2.x && point1.x > point3.x)largestX = (float) point1.x;
        else if(point2.x > point1.x && point2.x > point3.x)largestX = (float) point2.x;
        else largestX = (float) point3.x;
        if(point1.x < point2.x && point1.x < point3.x)smallestX = (float) point1.x;
        else if(point2.x < point1.x && point2.x < point3.x)smallestX = (float) point2.x;
        else smallestX = (float) point3.x;
        if(point1.y > point2.y && point1.y > point3.y)largestY = (float) point1.y;
        else if(point2.y > point1.y && point2.y > point3.y)largestY = (float) point2.y;
        else largestY = (float) point3.y;
        if(point1.y < point2.y && point1.y < point3.y)smallestY = (float) point1.y;
        else if(point2.y < point1.y && point2.y < point3.y)smallestY = (float) point2.y;
        else smallestY = (float) point3.y;

        gameObject.model = new Quad(0, 0, largestX - smallestX, largestY - smallestY, texture); // creates model

        world.addBody(gameObject.body); // adds body to world

        return gameObject; // returns gameObject
    }

    public void render(Window window){ // renders model to window
        Transform transform = body.getTransform();
        model.translate((float) transform.getTranslation().x, (float) transform.getTranslation().y);
        model.rotate((float) Math.toDegrees(transform.getRotation()));
        window.queueModel(model);
    }

    public float getRotation(){ // gets rotation of body
        return (float) Math.toDegrees(body.getTransform().getRotation());
    }

    public Vector2 getPosition(){ // gets position vector of body
        return body.getTransform().getTranslation();
    }

    public void applyForce(Vector2 force){ // applies force to body at center of mass
        body.applyForce(force);
    }

    public void applyForce(Vector2 force, Vector2 point){ // applies force to body at point
        body.applyForce(force, point);
    }

    public void applyTorque(float torque){ // applies torque to body
        body.applyTorque(torque);

    }

    public void clearForce(){ // clears all of a bodies applied forces
        body.clearForce();
    }

    public void clearTorque(){ // clears all of a bodies applied torque
        body.clearTorque();
    }

    public void changeTexture(Texture texture){ // changes the texture of model
        model.texture = texture;
    }

    public Body getBody(){ // returns body
        return body;
    }

    public Model getModel(){ // returns model
        return model;
    }
}
