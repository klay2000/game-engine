#version 120

attribute vec3 vertices;
attribute vec2 textures;

uniform mat4 modelMatrix;
uniform mat4 camMatrix;

varying vec2 tex_coords;

void main() {
    tex_coords = textures;
    gl_Position = camMatrix * modelMatrix * vec4(vertices, 1.0);
}